<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "GW".
 *
 * @property int $ID
 * @property string $GATEWAY
 */
class Centro extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'GW';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['GATEWAY'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'GATEWAY' => 'Gateway',
        ];
    }
}
