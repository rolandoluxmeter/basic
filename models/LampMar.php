<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "LAMPMAR".
 *
 * @property int $ID
 * @property string $DATE
 * @property string $TIME
 * @property string $JAULA
 * @property string $ESTADO
 * @property int $LAMPARA
 * @property string $NSERIE
 */
class LampMar extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'LAMPMAR';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['DATE', 'TIME'], 'safe'],
            [['LAMPARA'], 'integer'],
            [['JAULA', 'ESTADO'], 'string', 'max' => 20],
            [['NSERIE'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'DATE' => 'Date',
            'TIME' => 'Time',
            'JAULA' => 'Jaula',
            'ESTADO' => 'Estado',
            'LAMPARA' => 'Lampara',
            'NSERIE' => 'Nserie',
        ];
    }
}
